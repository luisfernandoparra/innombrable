package io.swagger.model.result.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.BAD_REQUEST)
public class IllegalColumnException extends RuntimeException {

	public IllegalColumnException(String errorMessage) {
		super(errorMessage);
		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
