package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2016-12-06T23:39:22.714Z")
public class AnalysisStatus<T,R> {

    private static final Logger logger=Logger.getLogger(AnalysisStatus.class.getCanonicalName());
    
    private String analysisId = null;
    private String issuingTimestamp = null;
    public static final String DEFAULT_SPEC_FILENAME="spec.json";
    public static final String DEFAULT_RESULT_FILENAME="result.json";
    
    

    public AnalysisStatus(){
        this((AnalysisSpecification)null);
    }
    
    public AnalysisStatus(File f) {
        this.analysisId = f.getName();
        this.issuingTimestamp = new Date(f.lastModified()).toString();
        this.updateStatus(f.getParentFile().getAbsolutePath());
    }

    public AnalysisStatus(AnalysisSpecification analysisSpec) {
        this.analysisId=UUID.randomUUID().toString();
        this.issuingTimestamp=new Date().toString();
        this.analysisSpecification=analysisSpec;
        this.analysisResult=null;
        this.currentStatus=CurrentStatusEnum.ISSUED;
    }

    private AnalysisSpecification<T> loadAnalysisSpec(File f) {
        AnalysisSpecification result=null;
        if(f.exists()){
            if(f.isDirectory())
                f=new File(f.getAbsolutePath()+"/"+DEFAULT_SPEC_FILENAME);
            if(f.exists()){
                logger.info("Loading status from: "+f.getAbsolutePath());
                try {
                    ObjectMapper om=new ObjectMapper();
                    result=(AnalysisSpecification<T>)om.readValue(f, AnalysisSpecification.class);
                } catch (IOException ex) {
                    logger.log(Level.SEVERE, "Unable to load analysis spec!", ex);
                }
            }
        }
        if(result==null)
            result=this.getAnalysisSpecification();
        return result;
    }

    private AnalysisResult<R> loadAnalysisResult(File f) {
        AnalysisResult result=null;
        if(f.exists()){
            if(f.isDirectory())
                f=new File(f.getAbsolutePath()+"/"+DEFAULT_RESULT_FILENAME);
            if(f.exists()){
                try {
                    ObjectMapper om=new ObjectMapper();
                    result=(AnalysisResult<R>)om.readValue(f, AnalysisResult.class);
                } catch (IOException ex) {
                    logger.log(Level.SEVERE, "Unable to load analysis spec!", ex);
                }
            }
        }
        if(result==null)
            result=this.getAnalysisResult();
        return result;
    }

    public void updateStatus(String path) {
        File f=new File(path+"/"+analysisId);
        this.analysisSpecification = loadAnalysisSpec(f);        
        this.analysisResult = loadAnalysisResult(f);

        if (analysisResult != null) {
            this.currentStatus = CurrentStatusEnum.DONE;
        } else if (analysisSpecification != null) {
            this.currentStatus = CurrentStatusEnum.EXECUTING;
        } else {
            this.currentStatus = CurrentStatusEnum.ISSUED;
        }
    }
    
    public void save(String path){
        saveAnalysisSpec(new File(path+"/"+analysisId+"/"+DEFAULT_SPEC_FILENAME));
        if (analysisResult != null) 
             saveAnalysisResult(new File(path+"/"+analysisId+"/"+DEFAULT_RESULT_FILENAME));
    }
    
    public void saveAnalysisSpec(File f)
    {
        try {
            f.getParentFile().mkdirs();
            f.createNewFile();
            ObjectMapper om=new ObjectMapper();
            om.writeValue(f, this.getAnalysisSpecification());
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }
    
    public void saveAnalysisResult(File f)
    {
        try {
            f.getParentFile().mkdirs();
            f.createNewFile();
            ObjectMapper om=new ObjectMapper();
            om.writeValue(f, this.getAnalysisResult());
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Gets or Sets currentStatus
     */
    public enum CurrentStatusEnum {
        ISSUED("issued"),
        EXECUTING("executing"),
        DONE("done");
        private String value;

        CurrentStatusEnum(String value) {
            this.value = value;
        }

        @Override
        @JsonValue
        public String toString() {
            return String.valueOf(value);
        }
    }

    private CurrentStatusEnum currentStatus = null;
    private AnalysisSpecification<T> analysisSpecification = null;
    private AnalysisResult<R> analysisResult = null;

    /**
     *
     */
    @JsonProperty("analysisId")
    public String getAnalysisId() {
        return analysisId;
    }

    public void setAnalysisId(String analysisId) {
        this.analysisId = analysisId;
    }

    /**
     *
     */
    @JsonProperty("issuingTimestamp")
    public String getIssuingTimestamp() {
        return issuingTimestamp;
    }

    public void setIssuingTimestamp(String issuingTimestamp) {
        this.issuingTimestamp = issuingTimestamp;
    }

    /**
     *
     */
    @JsonProperty("currentStatus")
    public CurrentStatusEnum getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(CurrentStatusEnum currentStatus) {
        this.currentStatus = currentStatus;
    }

    /**
     *
     */
    @JsonProperty("analysisSpecification")
    public AnalysisSpecification<T> getAnalysisSpecification() {
        return analysisSpecification;
    }

    public void setAnalysisSpecification(AnalysisSpecification<T> analysisSpecification) {
        this.analysisSpecification = analysisSpecification;
    }

    /**
     *
     */
    @JsonProperty("analysisResult")
    public AnalysisResult<R> getAnalysisResult() {
        return analysisResult;
    }

    public void setAnalysisResult(AnalysisResult<R> analysisResult) {
        this.analysisResult = analysisResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AnalysisStatus analysisStatus = (AnalysisStatus) o;
        return Objects.equals(analysisId, analysisStatus.analysisId)
                && Objects.equals(issuingTimestamp, analysisStatus.issuingTimestamp)
                && Objects.equals(currentStatus, analysisStatus.currentStatus)
                && Objects.equals(analysisSpecification, analysisStatus.analysisSpecification)
                && Objects.equals(analysisResult, analysisStatus.analysisResult);
    }

    @Override
    public int hashCode() {
        return Objects.hash(analysisId, issuingTimestamp, currentStatus, analysisSpecification, analysisResult);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class AnalysisStatus {\n");

        sb.append("    analysisId: ").append(toIndentedString(analysisId)).append("\n");
        sb.append("    issuingTimestamp: ").append(toIndentedString(issuingTimestamp)).append("\n");
        sb.append("    currentStatus: ").append(toIndentedString(currentStatus)).append("\n");
        sb.append("    analysisSpecification: ").append(toIndentedString(analysisSpecification)).append("\n");
        sb.append("    analysisResult: ").append(toIndentedString(analysisResult)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
