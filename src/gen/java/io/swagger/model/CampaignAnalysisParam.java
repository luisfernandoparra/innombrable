package io.swagger.model;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author japarejo
 */
public class CampaignAnalysisParam {
    String name ="bankimia";
    Integer[] years={2016};
    String[] months={"abril","mayo","junio","julio"};
    /*String [] columnasValiosas={"id_channel","bbdd_subscribed","SEGM_PUBLISHERS",
                                 "DEVICES","ESP_OPENED","DDBB_OPENED","SEGM_CATEGORIA",
                                 "marketing_opener","MARKETING_CLICKER","MARKETING_PURCHASER",
                                 "USERS_LOCATION","IP_COUNTRY","IP_CITY","IP_PROVIDER",
                                 "GENDER","SEGMPURCH_CATEGORIES","SEGMPURCH_PRODUCTS",
                                 "SEGM_CATEGORIA","CLICKER_SEGM_CATEGORIA","SEGM_AGE_RANGE",
                                 "EMAIL_DOMAIN","SEGMPURCH_TOTAL","IP_REGION","IP_ZIP","P161"};*/
    String [] columnasValiosas={"partner_id",                      //Identificador de afiliado (organización a la que se pagar por el tráfico entrante).
                                 "external_advertiser_id",          // Identificador de anunciante que contrata la campaña
                                 "adspace_id",                      // Id de la BD desde la que viene el registro
                                 "conversion_id",                   // Id del cliente (externo) de manera que lo podemo scotejar.  Esta es una columna única por anunciante.
                                 "conversion_product_category_id",  // Id de la categoría de producto de la conversión
                                 "conversion_tracking_time",        // Timestamp de la conversión
                                 "click_time",                      // Timestamp del momento de click en la creatividad
                                 "click_referrer",                  // Fuente de tráfico de origen de la conversión
                                 "click_admedia_id",                // Identificador de la creatividad que origina la conversión
                                 "advertiser_label",                // Nombre del anunciante
                                 "partner_adspace_label",           // Nombre de la BD desde la que vienen los datos
                                 "device_type",                     // Tipo de dispositivo desde el que se hace la conversión
                                 "click_subid_6"                    // Channel ID, identificador que nos permite hacer outher jooin con la BD de tracking.
                                         };
    private Map<String,String[]> filtering = new HashMap<String,String[]>();
    private Integer minimumData = 100;
    private Integer maximumData = 2000;
    
	public Integer getMinimumData() {
		return minimumData;
	}
	public void setMinimumData(Integer minimumData) {
		this.minimumData = minimumData;
	}
	public Integer getMaximumData() {
		return maximumData;
	}
	public void setMaximumData(Integer maximumData) {
		this.maximumData = maximumData;
	}
	public Map<String, String[]> getFiltering() {
		return filtering;
	}
	public void setFiltering(Map<String, String[]> filtering) {
		this.filtering = filtering;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer[] getYears() {
		return years;
	}
	public void setYears(Integer[] years) {
		this.years = years;
	}
	public String[] getMonths() {
		return months;
	}
	public void setMonths(String[] months) {
		this.months = months;
	}
	public String[] getColumnasValiosas() {
		return columnasValiosas;
	}
	public void setColumnasValiosas(String[] columnasValiosas) {
		this.columnasValiosas = columnasValiosas;
	}
    
    
}
