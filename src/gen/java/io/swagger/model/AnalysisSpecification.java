package io.swagger.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2016-12-06T23:39:22.714Z")
public class AnalysisSpecification<T>   {
  

  /**
   * Gets or Sets analysisType
   */
  public enum AnalysisTypeEnum {
    EchoTest("EchoTest"),
    Category("Category"),
    Campaign("Campaign");
    private String value;

    AnalysisTypeEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }
  }

  private AnalysisTypeEnum analysisType = null;
  private String callbackURL = null;
  private long waitingTime = 15000;
  private T parameters = null;

  /**
   **/
  
  @JsonProperty("analysisType")
  public AnalysisTypeEnum getAnalysisType() {
    return analysisType;
  }
  public void setAnalysisType(AnalysisTypeEnum analysisType) {
    this.analysisType = analysisType;
  }

  /**
   **/
  
  @JsonProperty("callbackURL")
  public String getCallbackURL() {
    return callbackURL;
  }
  public void setCallbackURL(String callbackURL) {
    this.callbackURL = callbackURL;
  }
  
  
  @JsonProperty("waitingMili")
	public long getWaitingTime() {
		return waitingTime;
	}

	public void setWaitingTime(long waitingTime) {
		this.waitingTime = waitingTime;
	}
/**
   **/
  
  @JsonProperty("parameters")
  public T getParameters() {
    return parameters;
  }
  public void setParameters(T parameters) {
    this.parameters = parameters;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnalysisSpecification analysisSpecification = (AnalysisSpecification) o;
    return Objects.equals(analysisType, analysisSpecification.analysisType) &&
        Objects.equals(callbackURL, analysisSpecification.callbackURL) &&
        Objects.equals(parameters, analysisSpecification.parameters);
  }

  @Override
  public int hashCode() {
    return Objects.hash(analysisType, callbackURL, parameters);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnalysisSpecification {\n");
    
    sb.append("    analysisType: ").append(toIndentedString(analysisType)).append("\n");
    sb.append("    callbackURL: ").append(toIndentedString(callbackURL)).append("\n");
    sb.append("    parameters: ").append(toIndentedString(parameters)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

