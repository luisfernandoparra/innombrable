package io.swagger.model.result;

import java.util.ArrayList;
import java.util.List;

import utils.MEStringUtils;

public class AnalysisResultValueClassification {

	private String row;
	private String column;
	private String value;
	
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	public String getColumn() {
		return column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public static AnalysisResultValueClassification[] parseClassification(List<String> classification) {
		List<AnalysisResultValueClassification> result = new ArrayList<AnalysisResultValueClassification>();
		String[] headers = classification.get(0).trim().split("[ ]");
		String col1=headers[0];
		String col2 = headers[3];
		String[] cols = {col1,col2};
		List<String> matrix = classification.subList(2, classification.size()-2);
		for(int i=0;i<matrix.size();i++){
			String[] splitted = matrix.get(i).split("[ ]");
			List<String> spl = (List<String>) MEStringUtils.clearEmpties(splitted);
			String row;
			String values[] = {spl.get(0),spl.get(1)};
			row = spl.get(2).trim().replace(":","");

			AnalysisResultValueClassification classification1 = new AnalysisResultValueClassification();
			classification1.setRow(row);
			classification1.setValue(values[0].trim().equals("")?"0":values[0]);
			classification1.setColumn(cols[0]);
			
			AnalysisResultValueClassification classification2 = new AnalysisResultValueClassification();
			classification2.setRow(row);
			classification2.setValue(values[1].trim().equals("")?"0":values[1]);
			classification2.setColumn(cols[1]);
			result.add(classification1);
			result.add(classification2);
		}
		return result.toArray(new AnalysisResultValueClassification[result.size()]);
	}
	
}
