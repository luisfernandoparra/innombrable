package io.swagger.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import io.swagger.annotations.Api;
import io.swagger.api.factories.StatusApiServiceFactory;
import io.swagger.model.BDConfigSpecification;
import io.swagger.model.file.structure.FileStructure;

@Path("/status")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2016-12-06T23:39:22.714Z")
@Api(value = "Status API", produces = "application/json")
public class StatusApi  {
   private final StatusApiService delegate = StatusApiServiceFactory.getStatusApi();

    @GET
    @Path("/bd")
    public Response statusBdGet(@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.statusBdGet(securityContext);
    }
    @PUT
    @Path("/bd")
    @Consumes({ "application/json" })
    public Response statusBdPut(BDConfigSpecification bdSpecification,@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.statusBdPut(bdSpecification,securityContext);
    }
    @GET
    @Path("/campaign") 
    public FileStructure statusCampaignGet(@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.statusCampaignGet(securityContext);
    }
    @GET   
    @Produces({ "application/json" })
    public Response statusGet(@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.statusGet(securityContext);
    }
}
