/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.us.isa.markexplorer;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 *
 * @author PO-029
 */
public class ExecuteAnalysisTasklet implements Tasklet {

    public static final RDelegate R=new RDelegate();
    private String path;
    /// Analysis type
    private String type;
    /// Analysis id
    private String id;                            
    private String callBackURL;
    private String params;
    
    
    public RepeatStatus execute(StepContribution sc, ChunkContext cc) throws Exception {
        List<String> fileContents;
        fileContents = IOUtils.readLines(new FileReader(new File(path+"/"+type+".R")));
        StringBuilder builder=new StringBuilder();
        addParamstoRScript(builder);
        for(String line:fileContents)
            builder.append(line);
        String result=R.executeScript(builder.toString(),"");
        invokeCallback(result);
        return RepeatStatus.FINISHED;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the callBackURL
     */
    public String getCallBackURL() {
        return callBackURL;
    }

    /**
     * @param callBackURL the callBackURL to set
     */
    public void setCallBackURL(String callBackURL) {
        this.callBackURL = callBackURL;
    }

    /**
     * @return the params
     */
    public String getParams() {
        return params;
    }

    /**
     * @param params the params to set
     */
    public void setParams(String params) {
        this.params = params;
    }

    private void addParamstoRScript(StringBuilder builder) {
        builder.append(params);
    }

    private void invokeCallback(String result) {
        
    }
    
    
    
}
