package io.swagger.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author japarejo
 */
public class CategoryAnalysisParam {
    /** Nombre de la columna a predecir, normalmente sera 'MARKETING_CLICKER'
     * o 'MARKETING_OPENER' */           
    private String objectiveColumn = "MARKETING_CLICKER";
    /** Porcentaje del fichero de datos a usar para entrenar los modelos*/
    private Double trainingPercentage = 0.2;
    /** Porcentaje del fichero de datos a usar para probar los modelos*/
    private Double testPercentage = 0.2;
    /** Columnas que se trataran para eliminar caracteres no legibles por el
     * algoritmo*/
    private String[] columnsToAsciify = {"IP_CITY","IP_PROVIDER","IP_REGION"};
    /** Columnas a normalizar usando un valor neutro para los nulos y blancos */
    private String[] columnsToNormalize = {};
    /** Columnas a transformar en factores */
    private String[] columnsToFactorize = {"marketing_opener","MARKETING_CLICKER","USERS_LOCATION", "IP_COUNTRY","IP_CITY","IP_PROVIDER",
                                "GENDER","EMAIL_DOMAIN","IP_REGION","IP_ZIP"};
    /** Columnas a usar para la pprediccion en el modelo si esa vacio se usaran 
     * todas las columnas disponibles.*/
    private String [] columnsToUse = {"marketing_opener", "MARKETING_CLICKER","USERS_LOCATION","IP_COUNTRY","IP_CITY",
                          "IP_PROVIDER","GENDER","SEGM_AGE_RANGE","EMAIL_DOMAIN","IP_REGION","IP_ZIP","P161"};
    private String [] discardedColumns={"MARKETING_PURCHASER","SEGMPURCH_CATEGORIES","SEGMPURCH_PRODUCTS","SEGM_CATEGORIA",
                          "CLICKER_SEGM_CATEGORIA", "SEGMPURCH_TOTAL"};
    private Map<String,String[]> filtering = new HashMap<String,String[]>();
    
    private Integer minimumData = 100;
    private Integer maximumData = 2000;
    
    public Integer getMinimumData() {
		return minimumData;
	}

	public void setMinimumData(Integer minimumData) {
		this.minimumData = minimumData;
	}

	public Integer getMaximumData() {
		return maximumData;
	}

	public void setMaximumData(Integer maximumData) {
		this.maximumData = maximumData;
	}

	public Map<String, String[]> getFiltering() {
		return filtering;
	}

	public void setFiltering(Map<String, String[]> filtering) {
		this.filtering = filtering;
	}

	/** Valor que permite hacer repetible la ejecucion de los algoritmos. */
     private int seed=0;

    /**
     * @return the objectiveColumn
     */
    public String getObjectiveColumn() {
        return objectiveColumn;
    }

    /**
     * @param objectiveColumn the objectiveColumn to set
     */
    public void setObjectiveColumn(String objectiveColumn) {
        this.objectiveColumn = objectiveColumn;
    }

    /**
     * @return the trainingPercentage
     */
    public Double getTrainingPercentage() {
        return trainingPercentage;
    }

    /**
     * @param trainingPercentage the trainingPercentage to set
     */
    public void setTrainingPercentage(Double trainingPercentage) {
        this.trainingPercentage = trainingPercentage;
    }

    /**
     * @return the testPercentage
     */
    public Double getTestPercentage() {
        return testPercentage;
    }

    /**
     * @param testPercentage the testPercentage to set
     */
    public void setTestPercentage(Double testPercentage) {
        this.testPercentage = testPercentage;
    }

    /**
     * @return the columnsToAsciify
     */
    public String[] getColumnsToAsciify() {
        return columnsToAsciify;
    }

    /**
     * @param columnsToAsciify the columnsToAsciify to set
     */
    public void setColumnsToAsciify(String[] columnsToAsciify) {
        this.columnsToAsciify = columnsToAsciify;
    }

    /**
     * @return the columnsToNormalize
     */
    public String[] getColumnsToNormalize() {
        return columnsToNormalize;
    }

    /**
     * @param columnsToNormalize the columnsToNormalize to set
     */
    public void setColumnsToNormalize(String[] columnsToNormalize) {
        this.columnsToNormalize = columnsToNormalize;
    }

    /**
     * @return the columnsToFactorize
     */
    public String[] getColumnsToFactorize() {
        return columnsToFactorize;
    }

    /**
     * @param columnsToFactorize the columnsToFactorize to set
     */
    public void setColumnsToFactorize(String[] columnsToFactorize) {
        this.columnsToFactorize = columnsToFactorize;
    }

    /**
     * @return the columnsToUse
     */
    public String[] getColumnsToUse() {
        return columnsToUse;
    }

    /**
     * @param columnsToUse the columnsToUse to set
     */
    public void setColumnsToUse(String[] columnsToUse) {
        this.columnsToUse = columnsToUse;
    }

    /**
     * @return the discardedColumns
     */
    public String[] getDiscardedColumns() {
        return discardedColumns;
    }

    /**
     * @param discardedColumns the discardedColumns to set
     */
    public void setDiscardedColumns(String[] discardedColumns) {
        this.discardedColumns = discardedColumns;
    }

    /**
     * @return the seed
     */
    public int getSeed() {
    	if(seed ==0){
	    	Random rand = new Random();
	    	this.seed=rand.nextInt();
    	}
        return seed;
    }

    /**
     * @param seed the seed to set
     */
    public void setSeed(int seed) {
        this.seed = seed;
    }
     
     
     
}
