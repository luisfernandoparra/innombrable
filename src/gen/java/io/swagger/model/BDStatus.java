package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.model.file.structure.FileStructure;

public class BDStatus {
	
	private String lastSource;
	private String lastUpdate;
	private String lastBackup;
	
	
	public BDStatus(FileStructure file) {
		super();
		this.lastSource = file.getFiles().get(0);
		this.lastUpdate = file.getFiles().get(1);
		this.lastBackup = file.getFiles().get(2);
		
	}
	
	public BDStatus(String lastSource, String lastUpdate, String lastBackup) {
		super();
		this.lastSource = lastSource;
		this.lastUpdate = lastUpdate;
		this.lastBackup = lastBackup;
	}

	public BDStatus(){
		super();
	}
	@JsonProperty("lastSource")
	public String getLastSource() {
		return lastSource;
	}
	public void setLastSource(String lastSource) {
		this.lastSource = lastSource;
	}
	
	@JsonProperty("lastUpdate")
	public String getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	@JsonProperty("lastBackup")
	public String getLastBackup() {
		return lastBackup;
	}
	public void setLastBackup(String lastBackup) {
		this.lastBackup = lastBackup;
	}
	
	public String toString(){
		String result;
		result = "[creation date: "+this.lastSource;
		result += "last Update: "+ this.lastUpdate;
		result += "last Backup: "+ this.lastBackup+"]";
		
		return result;
	}
	
	

}
