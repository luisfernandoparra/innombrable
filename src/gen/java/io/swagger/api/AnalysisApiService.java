package io.swagger.api;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.SecurityContext;

import io.swagger.model.AnalysisSpecification;
import io.swagger.model.AnalysisStatus;
import io.swagger.model.CampaignAnalysisParam;
import io.swagger.model.CategoryAnalysisParam;
import io.swagger.model.file.structure.FileStructure;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2016-12-06T23:39:22.714Z")
@SuppressWarnings("rawtypes")
public interface AnalysisApiService {
      public abstract String[] analysisGet(SecurityContext securityContext,HttpServletRequest request)
      throws NotFoundException;
      
	public abstract AnalysisStatus analysisIdGet(String id,SecurityContext securityContext, HttpServletRequest request)
      throws NotFoundException;
      public abstract AnalysisStatus analysisPost(AnalysisSpecification analysisSpec,SecurityContext securityContext,HttpServletRequest request)
      throws NotFoundException;
      public abstract AnalysisStatus analysisSegmentationCampaignNamePost(AnalysisSpecification<CampaignAnalysisParam> analysisSpec,String name,SecurityContext securityContext,HttpServletRequest request)
      throws NotFoundException;
      public abstract AnalysisStatus analysisSegmentationCategoryIdPost(AnalysisSpecification<CategoryAnalysisParam> analysisSpec,String id,SecurityContext securityContext,HttpServletRequest request)
      throws NotFoundException;
      public abstract FileStructure analysisGetCampaigns(SecurityContext securityContext)
    throws NotFoundException;
}
