package io.swagger.model.file.structure;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.swagger.model.AnalysisSpecification.AnalysisTypeEnum;

public class FileStructure {
	private String name;
	private List<String> files;
	private List<FileStructure> folders;
	public FileStructure(File f, boolean onlyArchives){
		files = new ArrayList<String>();
		folders = new ArrayList<FileStructure>();
		
		name = f.getName();
		
		for(File tem:f.listFiles()){
			if(tem.isDirectory()&&!onlyArchives){
				folders.add(new FileStructure(tem));
			}else if(tem.isFile()){
				files.add(tem.getName());
				
			}
		}
	}
	
	public FileStructure(File f) {
		FileStructure fs= new FileStructure(f,false);
		this.name = fs.getName();
		this.files = fs.getFiles();
		this.folders = fs.getFolders();
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getFiles() {
		return files;
	}
	public void setFiles(List<String> files) {
		this.files = files;
	}
	public List<FileStructure> getFolders() {
		return folders;
	}
	public void setFolders(List<FileStructure> folders) {
		this.folders = folders;
	}
	
}
