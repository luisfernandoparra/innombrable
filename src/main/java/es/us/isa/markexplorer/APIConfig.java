/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.us.isa.markexplorer;

import io.swagger.api.AnalysisApi;
import io.swagger.api.StatusApi;
import io.swagger.jaxrs.config.BeanConfig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.wadl.internal.WadlResource;
import org.springframework.context.annotation.Configuration;


@Configuration
@ApplicationPath("/rest")
public class APIConfig extends ResourceConfig {
    public APIConfig() {        
        
        register(AnalysisApi.class);
        register(StatusApi.class);                
        register(WadlResource.class);       
        register(io.swagger.jaxrs.listing.ApiListingResource.class);
        register(io.swagger.jaxrs.listing.SwaggerSerializers.class);        
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setTitle("MARKExplorer API");
        beanConfig.setVersion("0.5");
        beanConfig.setContact("José Antonio Parejo <japarejo@us.es>");
        beanConfig.setSchemes(new String[]{"http"});               
        beanConfig.setHost(getIp()+":8080");
        //beanConfig.setHost("139.162.201.242:8080");
        beanConfig.setBasePath("/rest");
        beanConfig.setResourcePackage("io.swagger.api");
        beanConfig.setScan(true);
    }
    
    public static String getIp()  {
    	String ip="150.214.188.133";
        
        BufferedReader in = null;
        try {
        	URL whatismyip = new URL("http://checkip.amazonaws.com");
        	in = new BufferedReader(new InputStreamReader(
                    whatismyip.openStream()));
            ip = in.readLine();            
        } catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ip;
    }
}
