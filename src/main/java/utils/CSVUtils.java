package utils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.enums.CSVReaderNullFieldIndicator;

import es.us.isa.markexplorer.api.impl.AnalysisApiServiceImpl;
import io.swagger.model.AnalysisSpecification.AnalysisTypeEnum;

public class CSVUtils {
	private String sourcePath;
	private String incrementalPath;

	public CSVUtils(String sourcePath, String incrementalPath) {
		super();
		this.sourcePath = sourcePath;
		this.incrementalPath = incrementalPath;
	}

	public CSVUtils(String incrementalPath) {
		super();
		this.sourcePath = new AnalysisApiServiceImpl().getDataPath(AnalysisTypeEnum.Category);
		this.incrementalPath = incrementalPath;
	}

	public CSVUtils(URL sourcePath2, URL incrementalPath2) {
		super();
		this.sourcePath = sourcePath2.toString();
		this.incrementalPath = incrementalPath2.toString();
	}

	public void mergeFiles() throws IOException {
		// First let's read both Files
		sourcePath = URLDecoder.decode(sourcePath, "UTF-8");
		incrementalPath = URLDecoder.decode(incrementalPath, "UTF-8");

		FileReader sourceReader = new FileReader(new File(sourcePath.toString().replace("file:", "")));
		FileReader incrementalReader = new FileReader(new File(incrementalPath).toString().replace("file:", ""));
		CSVReader sourceCsvReader = new CSVReaderBuilder(sourceReader)
				.withFieldAsNull(CSVReaderNullFieldIndicator.EMPTY_SEPARATORS).build();
		CSVReader incrementalCsvReader = new CSVReaderBuilder(incrementalReader)
				.withFieldAsNull(CSVReaderNullFieldIndicator.EMPTY_SEPARATORS).build();
		List<String[]> incrementalAll = incrementalCsvReader.readAll();
		List<String[]> sourceAll = sourceCsvReader.readAll();
		
		List<String[]> updatedSource = null;

		Boolean columnsChanged =  checkHeaders(incrementalAll.get(0), sourceAll.get(0));
		if (columnsChanged) {
			//updatedSource = mergeColumnsChanged(incrementalAll, sourceAll);
		} else {
			updatedSource = mergeColumnsUnchanged(incrementalAll, sourceAll);
		}
			sourceCsvReader.close();
			String s =sourcePath.toString().replace("file:", "");
		  CSVWriter saver = new CSVWriter(new FileWriter(new File(s)));
		  saver.writeAll(updatedSource,false);
		
		  saver.close();
		 
	}

	private Boolean checkHeaders(String[] strings, String[] strings2) {
		Boolean result;
		result = false;
		if(strings.length == strings2.length){
			for(int i=0;i<strings.length;i++){
				if(strings[i]!=strings[i]){
					result = true;
					break;
				}
			}
		}
		return result;
		
	}
	/*public void replaceSource(String newSource){
		String newPath= URLDecoder.decode(newSource, "UTF-8");
		sourcePath = URLDecoder.decode(sourcePath, "UTF-8");
		FileReader sourceReader = new FileReader(sourcePath);
		CSVReader sourceCsvReader = new CSVReaderBuilder(sourceReader)
				.withFieldAsNull(CSVReaderNullFieldIndicator.EMPTY_SEPARATORS).build();
		List<String[]> lines = sourceCsvReader.readAll();
		
		Files.copy(newSource, out,StandardCopyOption.REPLACE_EXISTING)
	}*/

	private List<String[]> mergeColumnsUnchanged(List<String[]> incrementalAll, List<String[]> sourceAll) throws IOException {
		// TODO Auto-generated method stub
		// indexing by id_channel
		for (String[] tem : incrementalAll) {
			int index = searchInSource(tem, sourceAll);
			if (index > -1 && index < sourceAll.size()) {
				sourceAll.set(index, tem);
			} else {
				sourceAll.add(tem);
			}
		}
		return sourceAll;
	}

	private int searchInSource(String[] row, List<String[]> sourceAll) throws IOException {
		int res = -1;
		for (String[] tem : sourceAll) {
			String idSource = tem[0];
			if (row[0].equals(idSource)) {
				res = sourceAll.indexOf(tem);
				break;
			}

		}
		return res;
	}

//	public static void main(String[] args) {
//		String source = new CSVUtils("", "").getClass().getClassLoader().getResource("data/trySource.csv").getPath();
//		String incremental = new CSVUtils("", "").getClass().getClassLoader().getResource("data/incremental.csv")
//				.getPath();
//		CSVUtils util = new CSVUtils(source, incremental);
//		try {
//			util.mergeFiles();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}


}
