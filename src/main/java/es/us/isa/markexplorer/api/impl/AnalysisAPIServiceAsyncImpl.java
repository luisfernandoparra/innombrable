/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.us.isa.markexplorer.api.impl;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.SecurityContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.swagger.api.NotFoundException;
import io.swagger.model.AnalysisResult;
import io.swagger.model.AnalysisSpecification;
import io.swagger.model.AnalysisStatus;
import io.swagger.model.AnalysisStatus.CurrentStatusEnum;

/**
 *
 * @author japarejo
 */
@Component
public class AnalysisAPIServiceAsyncImpl extends AnalysisApiServiceImpl {

    private static final Logger logger=Logger.getLogger(AnalysisAPIServiceAsyncImpl.class.getName());
    
   // private RDelegate R;

    @Autowired
    AsyncAnalysisExecution asynchronous;
    
    public AnalysisAPIServiceAsyncImpl() {
      //  R = new RDelegate();
    }

    @Override
    public AnalysisStatus analysisPost(AnalysisSpecification analysisSpec, SecurityContext securityContext, HttpServletRequest request) throws NotFoundException{
        AnalysisStatus status = new AnalysisStatus(analysisSpec);
        Long wait = analysisSpec.getWaitingTime()>=0? analysisSpec.getWaitingTime():MAX_DURATION;
        status.setAnalysisResult(new AnalysisResult(null,"The analysis has taken more than "+wait+" milisec."));
        status.setCurrentStatus(CurrentStatusEnum.EXECUTING);
        long startTime = System.currentTimeMillis();
        String path = getPath(request);
        String datapah = getDataPath(analysisSpec.getAnalysisType());
        status.save(path);
        try {
            Object resultVariable=request.getAttribute("resultVariable").toString();
            String resultVariableName=resultVariable!=null?resultVariable.toString():null;
            Future<AnalysisStatus> futureStatus = asynchronous.executeAnalysis(status, path,resultVariableName,datapah);
            while (System.currentTimeMillis() - startTime < wait && !futureStatus.isDone()) {
                try {
                    Thread.sleep(SLEEP_PERDIOD);
                } catch (InterruptedException ex) {
                    logger.log(Level.SEVERE, null, ex);
                    status.setAnalysisResult(new AnalysisResult(null,"There was an error waiting for async"));
                }
            }
            if (futureStatus.isDone()) {
                try {
                    status = futureStatus.get(); 
                }catch (ExecutionException ex) {
                    logger.log(Level.SEVERE, null, ex);
                    status.setAnalysisResult(new AnalysisResult(null,"There was an error getting the status"));
                }
            } 
        }
        catch (InterruptedException ex) {
            logger.log(Level.SEVERE, null, ex);
            status.setAnalysisResult(new AnalysisResult(null,"There was an error with the async execution"));
        }
      
        return status;
    }
    
   

}
