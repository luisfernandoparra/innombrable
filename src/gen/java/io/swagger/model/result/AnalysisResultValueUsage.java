package io.swagger.model.result;

import java.util.List;

import utils.MEStringUtils;

public class AnalysisResultValueUsage {
	private String percentage;
	private String column;
	
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	public String getColumn() {
		return column;
	}
	public void setColumn(String column) {
		this.column = column;
	}
	public static AnalysisResultValueUsage[] parseUsage(List<String> usage) {
		
		usage = (List<String>) MEStringUtils.clearEmpties(usage);
		
		usage.remove(0);
		usage.remove(usage.size()-1);
		AnalysisResultValueUsage[] result = new AnalysisResultValueUsage[usage.size()];
		for(int i =0;i<usage.size();i++){
			List<String> data=(List<String>) MEStringUtils.clearEmpties(usage.get(i).trim().split("[\t]"));
			AnalysisResultValueUsage newUsage = new AnalysisResultValueUsage();
			newUsage.setPercentage(data.get(0));
			newUsage.setColumn(data.get(1));
			result[i]=newUsage;
		} 
		return result;
	}
		
}
