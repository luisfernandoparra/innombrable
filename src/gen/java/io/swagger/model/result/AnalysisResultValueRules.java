package io.swagger.model.result;

import java.util.ArrayList;
import java.util.List;

import io.swagger.model.result.exceptions.ParsingErrorException;
import utils.MEStringUtils;

public class AnalysisResultValueRules {
	private String n;
	private String lift;
	private String[] variables;
	private Double cost;
	private String classified;
	public static final String ERROR_MESSAGE = "Empty rules, please check the inputs";
	
	
	public String getClassified() {
		return classified;
	}
	public void setClassified(String classified) {
		this.classified = classified;
	}
	public String getN() {
		return n;
	}
	public void setN(String n) {
		this.n = n;
	}
	public String getLift() {
		return lift;
	}
	public void setLift(String lift) {
		this.lift = lift;
	}
	public String[] getVariables() {
		return variables;
	}
	public void setVariables(String[] variables) {
		this.variables = variables;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public static AnalysisResultValueRules[] parseRules(List<String> rules) {
		List<AnalysisResultValueRules> result = new ArrayList<AnalysisResultValueRules>();
		rules = (List<String>) MEStringUtils.clearEmpties(rules);
		if(rules.isEmpty())
			throw new ParsingErrorException(ERROR_MESSAGE);
		rules.remove(0);
		for(int i =0;i<rules.size();i++){
			List<String> rule = new ArrayList<String>();
			if(rules.get(i).contains("Rule")){
				rule.add(rules.get(i));
				int j=i+1;
				while(j<rules.size()&&!rules.get(j).contains("Rule")){
					rule.add(rules.get(j));
					j++;
				}
				result.add(parseSingleRule(rule));
			}
			
		}
		
		
		return result.toArray(new AnalysisResultValueRules[result.size()]);
	}
	private static AnalysisResultValueRules parseSingleRule(List<String> rule) {
		AnalysisResultValueRules result = new AnalysisResultValueRules();
		String n = "";
		String lift ="";
		Double cost = 0.;
		String classified = "";
		List<String> plainVars = new ArrayList<String>();
		rule = (List<String>) MEStringUtils.clearEmpties(rule);
		for(String tem:rule){
			
			String[] split =tem.trim().split("[ ]");
			List<String> cleaned = (List<String>) MEStringUtils.clearEmpties(split);
			
			
			if(tem.contains("Rule")){
				 n = cleaned.get(2).replace("(","").replace(",","");
				 lift = cleaned.get(4).replace(")","");
			}else if(tem.contains("class")){
				String coste = cleaned.get(3).replaceAll("[\\[\\]]", "");
				classified = cleaned.get(2);
				cost = new Double(coste);
			}else{ //en este caso son variables
				plainVars.add(tem.trim());
			}
			
		}
		List<String> vars = new ArrayList<String>();
		for(int i= 0;i<plainVars.size();i++){
			String varRes;
			if(plainVars.get(i).contains(" in ")){
				 varRes = "";
				for(int j = i;i<plainVars.size();j++){
					varRes +=plainVars.get(j);
					if(plainVars.get(j).contains("}")){
						i = j;
						break;
					}
				}
			
			}else{
				varRes= plainVars.get(i);
			}
			
			vars.add(varRes);
		}
		
		
		result.setN(n);
		result.setLift(lift);
		result.setCost(cost);
		result.setVariables(vars.toArray(new String[vars.size()])); 
		result.setClassified(classified);
		return result;
	}
	
	
}
