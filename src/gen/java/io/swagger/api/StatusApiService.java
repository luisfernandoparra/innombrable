package io.swagger.api;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import io.swagger.model.BDConfigSpecification;
import io.swagger.model.file.structure.FileStructure;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2016-12-06T23:39:22.714Z")
public interface StatusApiService {
      public abstract Response statusBdGet(SecurityContext securityContext)
      throws NotFoundException;
      public abstract Response statusBdPut(BDConfigSpecification bdSpecification, SecurityContext securityContext)
      throws NotFoundException;
      public abstract FileStructure statusCampaignGet(SecurityContext securityContext)
      throws NotFoundException;
      public abstract Response statusGet(SecurityContext securityContext)
      throws NotFoundException;
}
