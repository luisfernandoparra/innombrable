package utils;
public class BinarySemaphore {
	private boolean locked = false;

	public BinarySemaphore(boolean locked) {
		this.locked = locked;
	}

	public synchronized void waitForNotify() throws InterruptedException {
		while (locked) {
			wait();
		}
		locked = true;
	}

	public synchronized void notifyToWakeup() {
		if (locked) {
			notify();
		}
		locked = false;
	}
}