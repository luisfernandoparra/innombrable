package io.swagger.model.result;

import java.util.List;

import io.swagger.model.result.exceptions.ParsingErrorException;
import utils.MEStringUtils;

public class AnalysisResultValueEvaluation {
	private Integer no;
	private String errors;
	private String errorPercentage;
	private Double cost;
	private AnalysisResultValueClassification[] classification = new AnalysisResultValueClassification[4];
	private AnalysisResultValueUsage[] usage;
	
	
	public String getErrorPercentage() {
		return errorPercentage;
	}
	public void setErrorPercentage(String errorPercentage) {
		this.errorPercentage = errorPercentage;
	}
	public Integer getNo() {
		return no;
	}
	public void setNo(Integer no) {
		this.no = no;
	}
	public String getErrors() {
		return errors;
	}
	public void setErrors(String errors) {
		this.errors = errors;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public AnalysisResultValueClassification[] getClassification() {
		return classification;
	}
	public void setClassification(AnalysisResultValueClassification[] classification) {
		this.classification = classification;
	}
	public AnalysisResultValueUsage[] getUsage() {
		return usage;
	}
	public void setUsage(AnalysisResultValueUsage[] usage) {
		this.usage = usage;
	}
	public static AnalysisResultValueEvaluation parseEvaluation(List<String> evaluation) {
		if(evaluation.isEmpty()||MEStringUtils.clearEmpties(evaluation).isEmpty()){
			throw new ParsingErrorException("Evaluation Empty: "+evaluation.toString());
		}
		AnalysisResultValueEvaluation result = new AnalysisResultValueEvaluation();
		evaluation.remove(0);
		evaluation.remove(0);
		Integer dataIndex = evaluation.indexOf("	    No      Errors  Cost")+2;
		Boolean hasCost = true;
		//Si no podemos indexarlo a la primera, hacemos un for más intensivo.
		if(dataIndex ==1){ //si es uno es que era -1
			for(String tem:evaluation){
				if(tem.contains("No")||tem.contains("Errors")){
					dataIndex = evaluation.indexOf(tem)+2;
				}
				if(!tem.contains("Cost")) hasCost = false;
			}			
		}
		String[] dataSplitted = evaluation.get(dataIndex).trim().replace("<<","").split("[ \\(]");
		List<String>dataCleaned = (List<String>) MEStringUtils.clearEmpties(dataSplitted);
		String no = dataCleaned.get(0);
		String errors =dataCleaned.get(1);
		String errorPercentage =dataCleaned.get(2).replace(")","");
		String cost =hasCost?dataCleaned.get(3):"-1";
		result.setNo(new Integer(no));
		result.setErrors(errors);
		result.setErrorPercentage(errorPercentage);
		result.setCost(new Double(cost));
		
		
		Integer indexClassification = evaluation.indexOf("	   (a)   (b)    <-classified as");
		Integer indexUsage = evaluation.indexOf("	Attribute usage:");
//		if(indexUsage <0) {indexUsage = indexClassification +5 };
		List<String> classification = evaluation.subList(indexClassification, indexUsage<0?indexClassification +5:indexUsage);
		if(!(indexUsage <0)){
			List<String> usage = evaluation.subList(indexUsage, evaluation.size());	
			result.setUsage(AnalysisResultValueUsage.parseUsage(usage));
		}
		
		result.setClassification(AnalysisResultValueClassification.parseClassification(classification));
		
		return result;
	}
	
	
	
}
