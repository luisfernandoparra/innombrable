package io.swagger.model;

import java.util.Objects;
import java.util.ArrayList;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.model.CampaignData;
import io.swagger.model.DBStatus;
import java.util.List;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2016-12-06T23:39:22.714Z")
public class StatusSummary   {
  
  private DBStatus db = null;
  private List<CampaignData> conversionData = new ArrayList<CampaignData>();

  /**
   **/
  
  @JsonProperty("db")
  public DBStatus getDb() {
    return db;
  }
  public void setDb(DBStatus db) {
    this.db = db;
  }

  /**
   **/
  
  @JsonProperty("conversionData")
  public List<CampaignData> getConversionData() {
    return conversionData;
  }
  public void setConversionData(List<CampaignData> conversionData) {
    this.conversionData = conversionData;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StatusSummary statusSummary = (StatusSummary) o;
    return Objects.equals(db, statusSummary.db) &&
        Objects.equals(conversionData, statusSummary.conversionData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(db, conversionData);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StatusSummary {\n");
    
    sb.append("    db: ").append(toIndentedString(db)).append("\n");
    sb.append("    conversionData: ").append(toIndentedString(conversionData)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

