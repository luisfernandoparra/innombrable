var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use( bodyParser.json({limit: '50mb'}) );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));
app.post('/receive', function(req,res){
  console.log('connection received');
  console.log(req.body.analysisResult);
console.log(req.body.currentStatus);
res.send("ok");
});

var listener=app.listen(process.env.PORT || 8088, function () {
  console.log('Listening on port ' + listener.address().port);
});
