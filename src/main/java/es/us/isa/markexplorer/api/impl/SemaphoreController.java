package es.us.isa.markexplorer.api.impl;

import java.util.concurrent.Semaphore;

import org.springframework.stereotype.Component;

@Component
public class SemaphoreController {

	private static SemaphoreController singleton;
	protected static Semaphore semaforo= new Semaphore(1,true);

	public static SemaphoreController getInstance(){
		if(singleton==null){
			singleton = new SemaphoreController();
		}
		return singleton;
	}
	private SemaphoreController(){
		super();
	}
	
	public void acquire() throws InterruptedException {
		SemaphoreController.semaforo.acquire();
    }

    public void release() {
    	SemaphoreController.semaforo.release();
    }
}
