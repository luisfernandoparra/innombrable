package io.swagger.api.factories;

import es.us.isa.markexplorer.api.impl.StatusApiServiceImpl;
import io.swagger.api.StatusApiService;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2016-12-06T23:39:22.714Z")
public class StatusApiServiceFactory {

   private final static StatusApiService service = new StatusApiServiceImpl();

   public static StatusApiService getStatusApi()
   {
      return service;
   }
}
