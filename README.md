# README #

MERKExplorerAPI repository

### What is this repository for? ###

An APY to invoke DM algorithms from SmartSegments (the Netsales CRM tool).
Version: 0.1


### How do I get set up? ###

This is a maven-based project that uses jaxrs through resteasy.
You do not need a database or configuration, but you will need to have R installed in the host machine.
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

japarejo@us.es  
ptrinidad@us.es