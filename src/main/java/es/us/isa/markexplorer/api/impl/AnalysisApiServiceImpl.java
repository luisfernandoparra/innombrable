package es.us.isa.markexplorer.api.impl;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.SecurityContext;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.api.AnalysisApiService;
import io.swagger.api.NotFoundException;
import io.swagger.model.AnalysisSpecification;
import io.swagger.model.AnalysisSpecification.AnalysisTypeEnum;
import io.swagger.model.AnalysisStatus;
import io.swagger.model.file.structure.FileStructure;
import utils.CSVManager;

@SuppressWarnings({ "rawtypes", "unchecked" })
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2016-12-06T23:39:22.714Z")
public class AnalysisApiServiceImpl implements AnalysisApiService {

    private static final Logger logger=Logger.getLogger(AnalysisApiService.class.getName());    
    
    @Autowired
    CSVManager manager;
    //@Autowired
    //private JobLauncher jobLauncher;

    //@Autowired
    //private Job job;
    protected long MAX_DURATION=15000;
    protected long SLEEP_PERDIOD=1000;
    
    
    public String[] analysisGet(SecurityContext securityContext, HttpServletRequest request)
            throws NotFoundException {
        File f = new File(getPath(request));
        if (!f.exists()) {
            f.mkdirs();
        }
        logger.info("Listing analyses from:"+f.getAbsolutePath());
        return f.list();
    }

    
    public AnalysisStatus analysisIdGet(String id, SecurityContext securityContext, HttpServletRequest request)
            throws NotFoundException {
        File f = new File(getPath(request) + "/"+id);
        if (!f.exists()) {
            throw new NotFoundException(404, "Unable to find analysis with id ='" + id + "'");
        }
        return new AnalysisStatus(f);
    }

    
    public AnalysisStatus analysisPost(AnalysisSpecification analysisSpec, SecurityContext securityContext, HttpServletRequest request)
            throws NotFoundException {
        // do some magic!
        //return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, "magic!")).build();
        return null;
    }

    
    public AnalysisStatus analysisSegmentationCampaignNamePost(AnalysisSpecification analysisSpec, String name, SecurityContext securityContext, HttpServletRequest request)
            throws NotFoundException {
    	AnalysisStatus result;
    	
        analysisSpec.setAnalysisType(AnalysisSpecification.AnalysisTypeEnum.Campaign);
//        analysisSpec.setParameters(name);
       
        request.setAttribute("resultVariable", "model$output");
        Map params=null;
        if(analysisSpec.getParameters() instanceof Map)
                params=(Map)analysisSpec.getParameters();        
        else{
            ObjectMapper om=new ObjectMapper();
            om.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
            params=(Map)om.convertValue(analysisSpec.getParameters(), Map.class);
        }
        params.put("name",name);
        analysisSpec.setParameters(params);
        result =analysisPost(analysisSpec, securityContext,request);
        
        return result;
    }

    

	public AnalysisStatus analysisSegmentationCategoryIdPost(AnalysisSpecification analysisSpec, String id, SecurityContext securityContext, HttpServletRequest request)
            throws NotFoundException {
    	
        analysisSpec.setAnalysisType(AnalysisSpecification.AnalysisTypeEnum.Category);
        request.setAttribute("resultVariable", "model$output");
        Map params=null;
        if(analysisSpec.getParameters() instanceof Map)
                params=(Map)analysisSpec.getParameters();        
        else{
            ObjectMapper om=new ObjectMapper();
            om.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
            params=(Map)om.convertValue(analysisSpec.getParameters(), Map.class);
        }
        params.put("category",id);
        analysisSpec.setParameters(params);
        AnalysisStatus status = analysisPost(analysisSpec, securityContext,request);
       
        return status;
    }
   

    @SuppressWarnings("unused")
	private AnalysisStatus issueAnalysis(AnalysisSpecification analysisSpec, HttpServletRequest request) {
        AnalysisStatus status = new AnalysisStatus(analysisSpec);
        long startTime=System.currentTimeMillis();
        String path=request.getServletContext().getRealPath("/");
        try {

            JobParameters param
                    = new JobParametersBuilder().
                            addString("path", path).
                            addString("id", status.getAnalysisId()).
                            addString("type", analysisSpec.getAnalysisType().name()).
                            addString("callBackURL", analysisSpec.getCallbackURL()).
                            addString("params", analysisSpec.getParameters().toString()).
                            toJobParameters();

            //JobExecution execution = jobLauncher.run(job, param);            
        } catch (Exception e) {
            e.printStackTrace();
        }
        while(System.currentTimeMillis()-startTime<MAX_DURATION && status.getCurrentStatus()!=AnalysisStatus.CurrentStatusEnum.DONE){
            try {
                Thread.sleep(SLEEP_PERDIOD);
            } catch (InterruptedException ex) {
                Logger.getLogger(AnalysisApiServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            status.updateStatus(path);
        }
        return status;
    }
    
    protected String getPath(HttpServletRequest request)
    {
        return request.getServletContext().getRealPath("/") + "/analyses";
    }
    public String getDataPath(AnalysisTypeEnum type)
    {    	
    	String result = "";    	
    	URL basePath=manager.getDefaultSourcePath();
    	if(basePath==null || basePath.equals("")){
    		URL path = this.getClass().getClassLoader().getResource("data/mark_traking_new.csv");	
    		if(type.equals(AnalysisTypeEnum.Campaign)){
    			path = this.getClass().getClassLoader().getResource("data/multiBase/campaigns");
    		}    	    		
    		try {
    			result = URLDecoder.decode(path.getPath(), "UTF-8");
    			result = result.substring(1, result.length());
    			result = result.replace("ile:/","");
    		} catch (UnsupportedEncodingException e) {
    			e.printStackTrace();
    		}
    	}else{
    		result=basePath+"/data/mark_traking_new.csv";
    		if(type.equals(AnalysisTypeEnum.Campaign)){
    			result=basePath+"data/multiBase/campaigns";
    		}    		
    	}
    	    	 		
        return result;
    }


	public FileStructure analysisGetCampaigns(SecurityContext securityContext)
			throws NotFoundException {
		//Set<String> res = new HashSet<String>();
		 File f = new File(getDataPath(AnalysisTypeEnum.Campaign));
		 FileStructure structure = new FileStructure(f);
		
	        return structure;
	}
	

}
