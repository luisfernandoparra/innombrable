package io.swagger.model.result;

import java.util.ArrayList;
import java.util.List;

import io.swagger.model.result.exceptions.IllegalColumnException;
import io.swagger.model.result.exceptions.ParsingErrorException;


public class AnalysisResultValue {
	private String executionDate;
	private String cases;
	private AnalysisResultValueRules[] rules;
	private AnalysisResultValueEvaluation evaluation;
	private String BD;
	private String Occurrences;
	
	
	public String getBD() {
		return BD;
	}
	public void setBD(String bD) {
		BD = bD;
	}
	public String getOccurrences() {
		return Occurrences;
	}
	public void setOccurrences(String occurrences) {
		Occurrences = occurrences;
	}
	public String getExecutionDate() {
		return executionDate;
	}
	public void setExecutionDate(String executionDate) {
		this.executionDate = executionDate;
	}
	public String getCases() {
		return cases;
	}
	public void setCases(String cases) {
		this.cases = cases;
	}
	public AnalysisResultValueRules[] getRules() {
		return rules;
	}
	public void setRules(AnalysisResultValueRules[] rules) {
		this.rules = rules;
	}
	public AnalysisResultValueEvaluation getEvaluation() {
		return evaluation;
	}
	public void setEvaluation(AnalysisResultValueEvaluation evaluation) {
		this.evaluation = evaluation;
	}
	

    public static AnalysisResultValue getValueFromResult(String output) throws IllegalColumnException{
    if(output.equals("{}")||output.contains("*** line")){
    	throw new ParsingErrorException("output was emtpy or was incorrect, output: "+output);
    }
    if(output.contains("[\"ha habido un error")){
    	throw new IllegalColumnException(output);
    }
    	AnalysisResultValue result = new AnalysisResultValue();
    	output = output.substring(2, output.length()-3);
    	output = output.replace("\\n", "\n").replace("\\t", "\t");
    	
    	String[] splitted = output.split("[\\n]");
    	String date = splitted[1].replace("C5.0 [Release 2.07 GPL Edition]", "").trim();
    	result.setExecutionDate(date);
    	
    	String cases = splitted[6].split("[ ]")[1];
    	result.setCases(cases);
     	List<String> rules = new ArrayList<String>();
     	List<String> evaluation = new ArrayList<String>();
     	int i = 9;
     	for(i=9;i<splitted.length;i++){
     		rules.add(splitted[i]);
     		if(splitted[i+1].contains("Default class")){
     			i+=2; 
     			break;
     		}
     	}
     	for(int j=i;j<splitted.length;j++){
     		evaluation.add(splitted[j]);
     	}
    	result.setRules(AnalysisResultValueRules.parseRules(rules));
    	result.setEvaluation(AnalysisResultValueEvaluation.parseEvaluation(evaluation));
    	return result;
    	
    }
    
   /* public static void main(String[] args) {
        String s ="[\nC5.0 [Release 2.07 GPL Edition]  \tTue Apr 04 11:00:06 2017\n-------------------------------\n\nClass specified by attribute `outcome'\n\nRead 140 cases (10 attributes) from undefined.data\nRead misclassification costs from undefined.costs\n\nRules:\n\nRule 1: (82/7, lift 1.1)\n\tIP_ZIP = NULL\n\t->  class 0  [0.905]\n\nRule 2: (24/4, lift 4.2)\n\tIP_ZIP in {0, 01070,31500, 03002,30006,35002,35260, 04007,\n                   08026,28923,29004, 08950, 09003, 14001, 28009, 28013,45005,\n                   28030,28045, 28045,33205, 28045,46940, 29002, 30006, 31004,\n                   41001, 43004,43120,43839,43883, 46021, 48006}\n\t->  class 1  [0.808]\n\nDefault class: 0\n\n\nEvaluation on training data (140 cases):\n\n\t           Rules         \n\t  -----------------------\n\t    No      Errors   Cost\n\n\t     2   11( 7.9%)   0.24   <<\n\n\n\t   (a)   (b)    <-classified as\n\t  ----  ----\n\t   109     4    (a): class 0\n\t     7    20    (b): class 1\n\n\n\tAttribute usage:\n\n\t 75.71%\tIP_ZIP\n\n\nTime: 0.0 secs\n]";
       // s ="[\nC5.0 [Release 2.07 GPL Edition]  \tWed Apr 05 09:32:05 2017\n-------------------------------\n\nClass specified by attribute `outcome'\n\nRead 140 cases (12 attributes) from undefined.data\nRead misclassification costs from undefined.costs\n\nRules:\n\nRule 1: (111/2, lift 1.2)\n\tP161 <= 0\n\t->  class 0  [0.973]\n\nRule 2: (29/2, lift 4.4)\n\tP161 > 0\n\t->  class 1  [0.903]\n\nDefault class: 0\n\n\nEvaluation on training data (140 cases):\n\n\t           Rules         \n\t  -----------------------\n\t    No      Errors   Cost\n\n\t     2    4( 2.9%)   0.07   <<\n\n\n\t   (a)   (b)    <-classified as\n\t  ----  ----\n\t   109     3    (a): class 0\n\t     2    27    (b): class 1\n\n\n\tAttribute usage:\n\n\t100.00%\tP161\n\n\nTime: 0.0 secs\n]";
        s="";
        AnalysisResultValue value = getValueFromResult(s);
        System.out.println("hahahaha");
    }*/

}
