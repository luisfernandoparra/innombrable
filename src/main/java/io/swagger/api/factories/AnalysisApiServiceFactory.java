package io.swagger.api.factories;

import es.us.isa.markexplorer.api.impl.AnalysisApiServiceImpl;
import io.swagger.api.AnalysisApiService;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2016-12-06T23:39:22.714Z")
public class AnalysisApiServiceFactory {

   private final static AnalysisApiService service = (AnalysisApiService) new AnalysisApiServiceImpl();

   public static AnalysisApiService getAnalysisApi()
   {
      return service;
   }
}
