#######################################################
# Fichero de analisis para la optimización de la     #
# conversión en campañas recurrentes creado por     #
# Jose Antonio Parejo (japarejo@us.es, @japarejo)     #
# En el contexto del proyecto MARKExplorer para       #
# NETSALES.                                           #
#######################################################
#------------------------------------------------------
# PARAMETROS GLOBALES: 
#------------------------------------------------------

#======================================================
# Librerias
#======================================================
# Instalacion y carga de librerias
#------------------------------------------------------
library(pacman)
p_load(devtools, stats,adabag, arules, C50, dplyr,e1071, igraph, mclust,splitstackshape,varSelRF,XML,splitstackshape,readr,jsonlite)
#======================================================
# Carga de datos
#======================================================
if(!exists("dataFilePath")){
  dataFilePath <-"/home/japarejo/Dropbox/proyectos/NetSales/datosAgosto/multiBase"  
}


#path <- "C:\\Users\\japarejo\\Dropbox\\proyectos\\NetSales\\datosAgosto\\multiBase\\"

if(!exists("months")){months <- c("marzo","abril","mayo","junio","julio","agosto")}
months <- setNames(months,months)

campaigns<-list.files(dataFilePath)
if(!exists("campaigns")){campaigns <- c("bankimia","creditea","gafases")}
campaigns <- setNames(campaigns,campaigns)

if(!exists("name")){name<-"bankimia"}

setwd(dataFilePath);


campaign<-campaigns[name]
#filename <- "bankimia_junio.xls"

#----------------------------------------------------------------------------------
# Lectura de datos de conversión en formato XLS (Excel) o CSV
#----------------------------------------------------------------------------------

dataByMonth <-  vector(mode="list", length=length(months))
names(dataByMonth)<- months
month <- months[1]
# Leemos los datos de los distintos meses
for(month in months){  
  for(year in years){
    filename<-paste(dataFilePath,"/",campaign,"/",year,"/",month,".csv",sep="")
    print(paste("Loading file",filename))
    # Descartamos el uso de xls
    #wb <- loadWorkbook(filename)
    #dataByMonth[[x]] <- readWorksheet(wb, sheet=1) 
    #dataByMonth[[x]] <- read.xls(filename, sheet=1, na.strings=c("", "NA","NULL"),header = TRUE)
    # Usamos csv
    dataByMonth[[month]] <- read.csv2(filename,stringsAsFactors=FALSE,na.strings=c("", "NA","NULL"))
  }
}
# Unimos los datos en un único data frame:
i <- 0
for(month in months){  
  if(i==0)
    myData <- dataByMonth[[month]]
  else
    myData <- rbind(myData,dataByMonth[[month]])
  i<- i + 1
}
print(paste("Número total de filas en los ficheros de conversión:",nrow(myData)));
# nos quedamos solo con los datos para los que el click_subid_6 no es nulo
myFilteredData<-subset(myData, (!is.na(myData$click_subid_6)))
# eliminamos los valores de prueba (Aquellos que tienen un valor "{{channel_id}}" en el campo click_subid_6):
myFilteredData<-myFilteredData[ ! myFilteredData$click_subid_6 %in% c("{{channel_id}}"), ]
print(paste("Numero total de filas en los ficheros de conversion que vienen de Netsales:",nrow(myFilteredData)));
#----------------------------------------------------------------------------------
# Lectura de datos en formato CSV
#----------------------------------------------------------------------------------
filename<-paste(substring(dataFilePath,1,75),"clean-mark_traking_new.csv",sep="")
fulldata <- read.csv2(filename,stringsAsFactors=FALSE,na.strings=c("", "NA","NULL"))           
joinedData <- fulldata[fulldata$id_channel %in% myFilteredData$click_subid_6,]
columnasValiosas<-c(
  "partner_id",                      #Identificador de afiliado (organización a la que se pagar por el tráfico entrante).
  "external_advertiser_id",         # Identificador de anunciante que contrata la campaña
  "adspace_id",                     # Id de la BD desde la que viene el registro
  "conversion_id",                 # Id del cliente (externo) de manera que lo podemo scotejar. 
  # Esta es una columna única por anunciante.
  "conversion_product_category_id", # Id de la categoria de producto de la conversión
  "conversion_tracking_time",       # Timestamp de la conversión
  "click_time",                     # Timestamp del momento de click en la creatividad
  "click_referrer",                 # Fuente de tráfico de origen de la conversión
  "click_admedia_id",               # Identificador de la creatividad que origina la conversión
  "advertiser_label",               # Nombre del anunciante
  "partner_adspace_label",          # Nombre de la BD desde la que vienen los datos
  "device_type",                   # Tipo de dispositivo desde el que se hace la conversión
  "click_subid_6"                  # Channel ID, identificador que nos permite hacer outher
  # join con la BD de tracking.
)

myData<-myData[,columnasValiosas]

# openers<- mydata[mydata$marketing_opener == 1,]
# print(paste("Openers: ",nrow(openers)))
# clickers <- mydata[mydata$MARKETING_CLICKER == 1,]
# print(paste("Clickers: ",nrow(clickers),fill=""))
# purchasers<- mydata[mydata$MARKETING_PURCHASER == 1,]
# print(paste("Purchasers: ",nrow(purchasers)))

# Categorias que son solo hojas:
filename<-paste(substring(dataFilePath,1,75),"categorias_hojas.csv",sep="")
categorias <- read.csv2(filename,stringsAsFactors=FALSE,sep = ",")
attach(myData)

#=====================================================
# Reestructuración de datos
#======================================================
randomSample<-fulldata[sample(nrow(fulldata),nrow(joinedData)),columnasValiosas]            
joinedData$MARKETING_PURCHASER=1;
randomSample$MARKETING_PURCHASER=0;
variablesToRecode <- c("bbdd_subscribed","SEGM_PUBLISHERS","DEVICES","ESP_OPENED","DDBB_OPENED","IP_CITY","SEGM_CATEGORIA")
for(x in variablesToRecode)
{ 
  
  joinedData[is.na(joinedData[,x]),x] <- "NONE"
  joinedData<- cSplit_e(joinedData,x,drop=TRUE,type="character",fill=0)
}

joinedData$MARKETING_CLICKER<-as.factor(joinedData$MARKETING_CLICKER);



for(x in variablesToRecode)
{ 
  
  randomSample[is.na(randomSample[,x]),x] <- "NONE"
  joinedData<- cSplit_e(joinedData,x,drop=TRUE,type="character",fill=0)
}
joinedData$MARKETING_PURCHASER <- as.factor(joinedData$MARKETING_PURCHASER);
model <- C5.0(MARKETING_PURCHASER ~ ., data=joinedData);

print("--------------------------------------")
print("Modelo calculado! A continuacion mostramos una representacion del mismo")
print("--------------------------------------")
summary(model)
print("--------------------------------------")