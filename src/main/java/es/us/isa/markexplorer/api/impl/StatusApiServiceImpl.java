package es.us.isa.markexplorer.api.impl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import io.swagger.api.ApiResponseMessage;
import io.swagger.api.NotFoundException;
import io.swagger.api.StatusApiService;
import io.swagger.model.BDConfigSpecification;
import io.swagger.model.BDConfigSpecification.UpdateType;
import io.swagger.model.BDStatus;
import io.swagger.model.file.structure.FileStructure;
import utils.CSVManager;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2016-12-06T23:39:22.714Z")
public class StatusApiServiceImpl  implements StatusApiService {
   
      private static final String ONDEVELOPMENT = "Still on development";
      
      
      private AnalysisApiServiceImpl analysisImpl = new AnalysisApiServiceImpl();

	public Response statusBdGet(SecurityContext securityContext) throws NotFoundException {
      Response result;
      
      CSVManager manager = CSVManager.getInstance();
      BDStatus res = manager.getCurrentStatus();
      result = Response.ok().entity(res).build();
    
      return result;
  }
      
      public Response statusBdPut(BDConfigSpecification bdSpecification,SecurityContext securityContext)
      throws NotFoundException {
    	  SemaphoreController semaforo = SemaphoreController.getInstance();
    	  try {
  			semaforo.acquire();
  		} catch (InterruptedException e) {
  			
  			e.printStackTrace();
  		}
    	  CSVManager manager = CSVManager.getInstance();
    	  BDStatus statusBD = new BDStatus();
    	  if(bdSpecification.getType().equals(UpdateType.Incremental)){
			try {
				manager.setIncrementalPath(bdSpecification.getPath());
				statusBD.setLastUpdate("Incremental correctly set to: "+manager.getDefaultIncrementalPath());
				manager.incrementalMerge();
				
			} catch (MalformedURLException e) {
				statusBD.setLastUpdate("The Incremental Path was incorrect, couldn't find file ("+e.getCause()+")");
				
			} catch (UnsupportedEncodingException e) {
				statusBD.setLastUpdate("The Incremental Path was incorrect, couldn't correctly encode path to UTF-8 ("+e.getCause()+")");
			} catch (IOException e) {
				statusBD.setLastUpdate("The Incremental Path was incorrect, couldn't find file ("+e.getCause()+")");
			}finally{
				semaforo.release();
			}
    	  }	else if(bdSpecification.getType().equals(UpdateType.Full)){
			try {
				manager.setSourcePath(bdSpecification.getPath());
				statusBD.setLastUpdate("Source correctly set to: "+manager.getDefaultSourcePath());
			} catch (MalformedURLException e) {
				
				statusBD.setLastUpdate("The Source Path was incorrect, couldn't find file ("+e.getCause()+")");
			}catch (UnsupportedEncodingException e) {
				statusBD.setLastUpdate("The Source Path was incorrect, couldn't correctly encode path to UTF-8 ("+e.getCause()+")");
			}catch (IOException e) {
				statusBD.setLastUpdate("The Source Path was incorrect, couldn't find file ("+e.getCause()+")");
			}finally{
				semaforo.release();
			}
    	  }
    	  
    	  Response result =Response.ok().entity(statusBD).build(); 
      return result;
  }
      
      public FileStructure statusCampaignGet(SecurityContext securityContext)
      throws NotFoundException {
  		 return analysisImpl.analysisGetCampaigns(securityContext);
  }
      
      public Response statusGet(SecurityContext securityContext)
      throws NotFoundException {    	
 	       return Response.ok().entity(new ApiResponseMessage(ApiResponseMessage.OK, ONDEVELOPMENT)).build();
  }
}
