package io.swagger.api;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;

import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.model.AnalysisSpecification;
import io.swagger.model.AnalysisStatus;
import io.swagger.model.CampaignAnalysisParam;
import io.swagger.model.CategoryAnalysisParam;
import io.swagger.model.file.structure.FileStructure;


@Path("/analysis")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2016-12-06T23:39:22.714Z")
@Api(value = "Analysis API", produces = "application/json")
public class AnalysisApi implements AnalysisApiService  {
  
  @Autowired 
  private AnalysisApiService delegate;

    @GET         
    @ApiOperation(value = "Gets the set of analyses performed", response = String[].class)
    @Produces({ "application/json" })
    public String[] analysisGet(@Context SecurityContext securityContext, @Context HttpServletRequest request)
    throws NotFoundException {
        return delegate.analysisGet(securityContext,request);
    }
    
    @GET
    @Path("/{id}")        
    public AnalysisStatus analysisIdGet( @PathParam("id") String id,@Context SecurityContext securityContext,@Context HttpServletRequest request)
    throws NotFoundException {
        return delegate.analysisIdGet(id,securityContext,request);
    }
    
    @GET
    @Path("/getCampaigns")        
    public FileStructure analysisGetCampaigns(@Context SecurityContext securityContext)
    throws NotFoundException {
        return delegate.analysisGetCampaigns(securityContext);
    }
    
    @POST        
    @Produces({ "application/json" })
    public AnalysisStatus analysisPost( AnalysisSpecification analysisSpec,@Context SecurityContext securityContext,@Context HttpServletRequest request)
    throws NotFoundException {
        return delegate.analysisPost(analysisSpec,securityContext,request);
    }
    
    @POST
    @Path("/segmentation/campaign/{name}")    
    @Produces({ "application/json" })
    public AnalysisStatus analysisSegmentationCampaignNamePost( AnalysisSpecification<CampaignAnalysisParam> analysisSpec, @PathParam("name") String name,@Context SecurityContext securityContext, @Context HttpServletRequest request)
    throws NotFoundException {
        return delegate.analysisSegmentationCampaignNamePost(analysisSpec,name,securityContext,request);
    }
    @POST
    @Path("/segmentation/category/{id}")
    
    @Produces({ "application/json" })
    public AnalysisStatus analysisSegmentationCategoryIdPost( AnalysisSpecification<CategoryAnalysisParam> analysisSpec, @PathParam("id") String id,@Context SecurityContext securityContext, @Context HttpServletRequest request)
    throws NotFoundException {
        return delegate.analysisSegmentationCategoryIdPost(analysisSpec,id,securityContext,request);
    }
}
