package io.swagger.model;

import java.util.Objects;
import java.util.ArrayList;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.model.AnalysisResult;
import io.swagger.model.SegmentationAnalysisSpecification;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2016-12-06T23:39:22.714Z")
public class SegmentationAnalysisStatus   {
  
  private Long analysisId = null;
  private String issuingTimestamp = null;

  /**
   * Gets or Sets currentStatus
   */
  public enum CurrentStatusEnum {
    ISSUED("issued"),

        EXECUTING("executing"),

        DONE("done");
    private String value;

    CurrentStatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }
  }

  private CurrentStatusEnum currentStatus = null;
  private SegmentationAnalysisSpecification analysisSpecification = null;
  private AnalysisResult analysisResult = null;

  /**
   **/
  
  @JsonProperty("analysisId")
  public Long getAnalysisId() {
    return analysisId;
  }
  public void setAnalysisId(Long analysisId) {
    this.analysisId = analysisId;
  }

  /**
   **/
  
  @JsonProperty("issuingTimestamp")
  public String getIssuingTimestamp() {
    return issuingTimestamp;
  }
  public void setIssuingTimestamp(String issuingTimestamp) {
    this.issuingTimestamp = issuingTimestamp;
  }

  /**
   **/
  
  @JsonProperty("currentStatus")
  public CurrentStatusEnum getCurrentStatus() {
    return currentStatus;
  }
  public void setCurrentStatus(CurrentStatusEnum currentStatus) {
    this.currentStatus = currentStatus;
  }

  /**
   **/
  
  @JsonProperty("analysisSpecification")
  public SegmentationAnalysisSpecification getAnalysisSpecification() {
    return analysisSpecification;
  }
  public void setAnalysisSpecification(SegmentationAnalysisSpecification analysisSpecification) {
    this.analysisSpecification = analysisSpecification;
  }

  /**
   **/
  
  @JsonProperty("analysisResult")
  public AnalysisResult getAnalysisResult() {
    return analysisResult;
  }
  public void setAnalysisResult(AnalysisResult analysisResult) {
    this.analysisResult = analysisResult;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SegmentationAnalysisStatus segmentationAnalysisStatus = (SegmentationAnalysisStatus) o;
    return Objects.equals(analysisId, segmentationAnalysisStatus.analysisId) &&
        Objects.equals(issuingTimestamp, segmentationAnalysisStatus.issuingTimestamp) &&
        Objects.equals(currentStatus, segmentationAnalysisStatus.currentStatus) &&
        Objects.equals(analysisSpecification, segmentationAnalysisStatus.analysisSpecification) &&
        Objects.equals(analysisResult, segmentationAnalysisStatus.analysisResult);
  }

  @Override
  public int hashCode() {
    return Objects.hash(analysisId, issuingTimestamp, currentStatus, analysisSpecification, analysisResult);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SegmentationAnalysisStatus {\n");
    
    sb.append("    analysisId: ").append(toIndentedString(analysisId)).append("\n");
    sb.append("    issuingTimestamp: ").append(toIndentedString(issuingTimestamp)).append("\n");
    sb.append("    currentStatus: ").append(toIndentedString(currentStatus)).append("\n");
    sb.append("    analysisSpecification: ").append(toIndentedString(analysisSpecification)).append("\n");
    sb.append("    analysisResult: ").append(toIndentedString(analysisResult)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

