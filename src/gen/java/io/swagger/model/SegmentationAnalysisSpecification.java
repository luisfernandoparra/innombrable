package io.swagger.model;

import java.util.Objects;
import java.util.ArrayList;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2016-12-06T23:39:22.714Z")
public class SegmentationAnalysisSpecification   {
  
  private String callbackURL = null;
  private String objectiveOfSegmentation = null;

  /**
   * Gets or Sets algorithm
   */
  public enum AlgorithmEnum {
    _0("C5.0");
    private String value;

    AlgorithmEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }
  }

  private AlgorithmEnum algorithm = null;

  /**
   **/
  
  @JsonProperty("callbackURL")
  public String getCallbackURL() {
    return callbackURL;
  }
  public void setCallbackURL(String callbackURL) {
    this.callbackURL = callbackURL;
  }

  /**
   **/
  
  @JsonProperty("objectiveOfSegmentation")
  public String getObjectiveOfSegmentation() {
    return objectiveOfSegmentation;
  }
  public void setObjectiveOfSegmentation(String objectiveOfSegmentation) {
    this.objectiveOfSegmentation = objectiveOfSegmentation;
  }

  /**
   **/
  
  @JsonProperty("algorithm")
  public AlgorithmEnum getAlgorithm() {
    return algorithm;
  }
  public void setAlgorithm(AlgorithmEnum algorithm) {
    this.algorithm = algorithm;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SegmentationAnalysisSpecification segmentationAnalysisSpecification = (SegmentationAnalysisSpecification) o;
    return Objects.equals(callbackURL, segmentationAnalysisSpecification.callbackURL) &&
        Objects.equals(objectiveOfSegmentation, segmentationAnalysisSpecification.objectiveOfSegmentation) &&
        Objects.equals(algorithm, segmentationAnalysisSpecification.algorithm);
  }

  @Override
  public int hashCode() {
    return Objects.hash(callbackURL, objectiveOfSegmentation, algorithm);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SegmentationAnalysisSpecification {\n");
    
    sb.append("    callbackURL: ").append(toIndentedString(callbackURL)).append("\n");
    sb.append("    objectiveOfSegmentation: ").append(toIndentedString(objectiveOfSegmentation)).append("\n");
    sb.append("    algorithm: ").append(toIndentedString(algorithm)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

