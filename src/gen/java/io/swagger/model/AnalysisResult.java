package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonValue;
import java.util.Objects;
import java.util.ArrayList;
import java.util.Date;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2016-12-06T23:39:22.714Z")
public class AnalysisResult<T>   {

    private T value;
    private String log;
    private String finishTimestamp;             

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
    
    public String getLog(){
        return log;
    }
    
    public void setLog(String log){
        this.log=log;
    }

    public String getFinishTimestamp() {
        return finishTimestamp;
    }

    public void setFinishTimestamp(String finishTimestamp) {
        this.finishTimestamp = finishTimestamp;
    }
    
    public AnalysisResult() {
        this(null,"");
        
    }
    public AnalysisResult(T value, String log) {
        this.finishTimestamp=new Date().toString();
        this.value=value;
        this.log=log;
    }
    

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnalysisResult analysisResult = (AnalysisResult) o;
    return true;
  }

  @Override
  public int hashCode() {
    return Objects.hash();
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnalysisResult {\n");
    
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

