package es.us.isa.markexplorer.api.impl;

import static java.lang.ClassLoader.getSystemClassLoader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.us.isa.markexplorer.RDelegate;
import io.swagger.model.AnalysisResult;
import io.swagger.model.AnalysisSpecification;
import io.swagger.model.AnalysisStatus;
import io.swagger.model.AnalysisStatus.CurrentStatusEnum;
import io.swagger.model.result.AnalysisResultValue;
import io.swagger.model.result.AnalysisResultValueRules;
import io.swagger.model.result.exceptions.IllegalColumnException;
import io.swagger.model.result.exceptions.ParsingErrorException;
@SuppressWarnings("rawtypes")
@Component
public class AsyncAnalysisExecution {
	
	 public static final double trainingIncrement = 0.5;
	  public static final double testIncrement = 0.4;
	  public static final double maximumDataIncrement = 300;
	private RDelegate R;
	private static final Logger logger=Logger.getLogger(AnalysisAPIServiceAsyncImpl.class.getName());
	
	public AsyncAnalysisExecution() {
		super();
		R =new RDelegate();
	}

	
	@Async
    public Future<AnalysisStatus> executeAnalysis(AnalysisStatus status, String path,String resultVar, String dataPath) throws InterruptedException {
		SemaphoreController semaforo = SemaphoreController.getInstance(); 
    	
    	try {
			semaforo.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			AnalysisStatus newStatus = executeScript(status,path,resultVar,dataPath);
			
			  //Ya tenemos nuestra respuesta, vamos a asegurarnos de que no es un "Empty Rules"
        	if(newStatus.getAnalysisResult().getLog().contains(AnalysisResultValueRules.ERROR_MESSAGE)){
        		System.out.println("recalculating ...");
        		
        		//Si detectamos un Empty Rules aumentamos el porcentaje de entrenamiento y volvemos a lanzar el script.
        		AnalysisSpecification newSpecification = addMoreData(status.getAnalysisSpecification());
        		newStatus.setAnalysisSpecification(newSpecification);
        		newStatus = executeScript(newStatus, path, resultVar, dataPath);
        		System.out.println("recalculated");
        	}
        	newStatus.setCurrentStatus(CurrentStatusEnum.DONE);
        	 invokeCallback(newStatus);
        	semaforo.release();
			return new AsyncResult<AnalysisStatus>(newStatus);
		} catch (IOException ex) {
			String errorMessage = "Unable to load analysis script: " + ex.getMessage();
			// logger.log(Level.SEVERE, errorMessage, ex);
			throw new InterruptedException(errorMessage);
		}finally{
			semaforo.release();
		}
    }
	 private AnalysisSpecification addMoreData(AnalysisSpecification oldSpecification){
	    	AnalysisSpecification result;
	    	result = oldSpecification;
	    	LinkedHashMap params = (LinkedHashMap) oldSpecification.getParameters();
	    	
	    	//Los valores normalizados, para aumentarlos en caso de necesidad.
	    	Double testPercentage =((Double) params.get("testPercentage"))>1?((Double) params.get("testPercentage"))/100:((Double) params.get("testPercentage"));
	    	Double trainingPercentage =((Double) params.get("trainingPercentage"))>1?((Double) params.get("trainingPercentage"))/100:((Double) params.get("trainingPercentage"));
	    	Integer maximumData=((Integer) params.get("maximumData"));
	    	if(testPercentage!=null){
	    		params.put("testPercentage", testPercentage+testIncrement>=1?1:testPercentage+testIncrement);
	    	}
	    	if(trainingPercentage!=null){
	    		params.put("trainingPercentage", trainingPercentage+trainingIncrement>=1?1:trainingPercentage+trainingIncrement);
	    	}
	    	if(maximumData!=null){
	    		params.put("maximumData", maximumData+maximumDataIncrement);
	    	}
	    	result.setParameters(params);
	    	
	    	return result;	
	    }
	public AnalysisStatus executeScript(AnalysisStatus status, String path,String resultVar, String dataPath) throws FileNotFoundException, IOException{
				System.out.println("script in execution");
	            ObjectMapper mapper=new ObjectMapper();
	            String script = loadScript(path, status.getAnalysisSpecification().getAnalysisType().toString(),status,dataPath);
	            String Rresult = R.executeScript(script, "");            
	            AnalysisResult result = new AnalysisResult(null,Rresult);
	            String error="";
	            if(resultVar!=null){
	            	error=R.getSession().getLastLogEntry();
	            	String var = R.variableAsJSON(resultVar);
	            	String rankingBD =null;
	            	String rankingOccurrences=null;
	            	try{
	            		rankingBD=R.eval("toJSON(ranking.names)").replace("[1]","").replace("[", "").replace("]", "");
	            		rankingOccurrences = R.eval("unname(ranking)").replace("[1]","");
	            	}catch(Exception e){
	            		logger.info("Unable to get Ranking info, error:"+e.getMessage());
	            		e.printStackTrace();
	            		logger.severe("PreviousError: "+error);
	            	}
	            	//rankingOccurrences=rankingOccurrences.replace("[1]","").replaceAll("\\\"", "\"").split("\n")[2];
	            	AnalysisResultValue value;
					
					try {
						value = AnalysisResultValue.getValueFromResult(var);
												
					}catch(IllegalColumnException e){
						value = new AnalysisResultValue();
						String log = e.getMessage() + " || ";
						log += result.getLog();
						result.setLog(log);
			        } 
					catch (ParsingErrorException e) {
						value = new AnalysisResultValue();
						String log = e.getMessage() + " || ";
						log += result.getLog();
						result.setLog(log);
					}catch(Exception e){
						value = new AnalysisResultValue();
						e.printStackTrace();
						result.setLog(error +"\n"+ R.getSession().getLastLogEntry());
					}
					value.setBD(rankingBD);
					value.setOccurrences(rankingOccurrences);					
	            	String json = mapper.writeValueAsString(value==null?"there was an error":value);
	            	
	                result.setValue(mapper.readValue(json, AnalysisResultValue.class));
	            }
	            status.setAnalysisResult(result);
	           
	            status.save(path);
	            R.clearData();
	           return status;
	}

	public String loadScript(String path, String type,AnalysisStatus status, String dataPath) throws FileNotFoundException, IOException {
        List<String> fileContents = null;
        System.out.println("Loadig: RScripts/"+type + ".R");
        InputStream is=getSystemClassLoader().getResourceAsStream( "RScripts/"+type + ".R");
        if(is==null){
        	System.out.println("ERROR! Unable to load: RScripts/"+type + ".R");
        	ClassPathResource classPathResource = new ClassPathResource("RScripts/"+type + ".R");        	
        	is= classPathResource.getInputStream();
        	if(is==null)
        		System.out.println("ERROR! Unable to load: RScripts/"+type + ".R even in second attemp! :-(");        	
        }
        fileContents = IOUtils.readLines(new InputStreamReader(is));
        StringBuilder builder = new StringBuilder();
        addParamstoRScript(builder,status,dataPath);
        for (String line : fileContents) {
            builder.append(line);
            builder.append("\n");
        }
        logger.info(builder.toString());
        return builder != null ? builder.toString() : "";
    }
	
	public void invokeCallback(AnalysisStatus analysisStatus) {        
        if(analysisStatus.getAnalysisSpecification().getCallbackURL()!=null && !"".equals(analysisStatus.getAnalysisSpecification().getCallbackURL()))
        {
            Client client = ClientBuilder.newClient();
            client.target(analysisStatus.getAnalysisSpecification().getCallbackURL())
            .request(MediaType.APPLICATION_JSON)
            .post(Entity.json(analysisStatus));        
        }
    }
	
	  private void addParamstoRScript(StringBuilder builder,AnalysisStatus status, String dataPath) {
	        Map params=(Map)status.getAnalysisSpecification().getParameters();
	        params.put("dataFilePath", dataPath);
	        for(Object key:params.keySet()){            
	            buildParamDefinitionInR(key,params.get(key),builder);            
	        }
	    }
	     
	    private void buildParamDefinitionInR(Object key,Object value,StringBuilder builder){
	            
	            if(value instanceof Collection){
	                builder.append(key.toString()+" <- c(");
	                Collection values=(Collection)value;
	                Iterator iterator=values.iterator();
	                while(iterator.hasNext()){
	                    builder.append(printValue("    ",iterator.next(),"")+(iterator.hasNext()?",":""));
	                }
	                builder.append(")\n");
	            }else if(value instanceof Map){
	            	builder.append(key.toString() + " <- list(");
	            	int j=0;
	            	Map map = (Map<String,String[]>) value;
	            	for(Object tem:map.entrySet()){
	            		Entry par = (Entry)tem;
	            		builder.append(par.getKey().toString()+"=c(");
	            		List<String> valores = (ArrayList<String>) par.getValue();
	            		int i =0;
	            		for(String valor:valores){
	            			if(i==valores.size()-1){
	            				builder.append("\""+valor.toString()+"\""+")");
	            			}else{
	            				builder.append("\""+valor.toString()+"\""+",");	
	            			}
	            			i++;
	            		}
	            		if(!(j==map.size()-1)){
	            			builder.append(",");	
	            		}
	            		
	            		j++;
	            	}
	            	builder.append(")\n");
	            }else 
	                builder.append(key.toString()+" <- "+printValue("",value,"")+"\n");                            
	    }
	    
	    
	    private String printValue(String prefix,Object value, String suffix){
	        String result=null;
	            if(value instanceof String) 
	                result=prefix+"'"+value.toString()+suffix+"'";
	            else
	                result=prefix+value.toString()+suffix;
	        return result;
	    }
	     

}
