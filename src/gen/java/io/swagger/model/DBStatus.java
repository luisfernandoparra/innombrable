package io.swagger.model;

import java.util.Objects;
import java.util.ArrayList;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2016-12-06T23:39:22.714Z")
public class DBStatus   {
  
  private String timestamp = null;
  private Long size = null;

  /**
   * Gets or Sets dbFormat
   */
  public enum DbFormatEnum {
    CSV("csv"),

        SQL("sql");
    private String value;

    DbFormatEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }
  }

  private DbFormatEnum dbFormat = null;

  /**
   **/
  
  @JsonProperty("timestamp")
  public String getTimestamp() {
    return timestamp;
  }
  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  /**
   **/
  
  @JsonProperty("size")
  public Long getSize() {
    return size;
  }
  public void setSize(Long size) {
    this.size = size;
  }

  /**
   **/
  
  @JsonProperty("dbFormat")
  public DbFormatEnum getDbFormat() {
    return dbFormat;
  }
  public void setDbFormat(DbFormatEnum dbFormat) {
    this.dbFormat = dbFormat;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DBStatus dbStatus = (DBStatus) o;
    return Objects.equals(timestamp, dbStatus.timestamp) &&
        Objects.equals(size, dbStatus.size) &&
        Objects.equals(dbFormat, dbStatus.dbFormat);
  }

  @Override
  public int hashCode() {
    return Objects.hash(timestamp, size, dbFormat);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DBStatus {\n");
    
    sb.append("    timestamp: ").append(toIndentedString(timestamp)).append("\n");
    sb.append("    size: ").append(toIndentedString(size)).append("\n");
    sb.append("    dbFormat: ").append(toIndentedString(dbFormat)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

