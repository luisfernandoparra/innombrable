package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

public class BDConfigSpecification {

	public enum UpdateType{
		Incremental("Incremental"),
	    Full("Full");
	    private String value;

	    UpdateType(String value) {
	      this.value = value;
	    }

	    @Override
	    @JsonValue
	    public String toString() {
	      return String.valueOf(value);
	    }
	}
	
	private UpdateType type = null;
	private String path = null;
	
	@JsonProperty("updateType")
	public UpdateType getType() {
		return type;
	}
	public void setType(UpdateType type) {
		this.type = type;
	}
	
	@JsonProperty("updatePath")
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BDConfigSpecification other = (BDConfigSpecification) obj;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		if (type != other.type)
			return false;
		return true;
	}
	 
	
}
