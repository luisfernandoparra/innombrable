package utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.util.Assert;

import io.swagger.model.BDStatus;
import io.swagger.model.file.structure.FileStructure;

public class CSVManager {

	private static CSVManager singleton;

	private URL sourcePath = null;//this.getClass().getClassLoader().getResource("data/2017-06-08.csv");
	private URL incrementalPath = null;//this.getClass().getClassLoader().getResource("data/2017-07-26.csv");
	public static String contextPath ;//= singleton.getClass().getClassLoader().getResourceAsStream("");
	private static String outBasePath = "http://wise-segments.com/crm/cron/process/sev-markExplorer/fIcchhs/";
	public final String PATHDETECTOR="target";
	public BDStatus currentStatus=new BDStatus("","","");
	public static final String IOERROR="IOerror";
	public static final String SEMAPHORERR="SemaphoreError";

	public static CSVManager getInstance() {
		if (singleton == null) {
			singleton = new CSVManager();
			try {
			CSVManager.contextPath = singleton.getClass().getClassLoader().getResource("data/").getPath();
			singleton.checkFiles();
			
			
			CSVManager.contextPath= URLDecoder.decode(CSVManager.contextPath,"UTF-8");
			} catch (UnsupportedEncodingException e1) {
				CSVManager.contextPath = singleton.getClass().getClassLoader().getResource("data/").getPath();
			} catch (MalformedURLException e) {
				CSVManager.contextPath = singleton.getClass().getClassLoader().getResource("data/").getPath();
			}
			
			singleton.currentStatus= new BDStatus(singleton.sourcePath.getPath(),singleton.incrementalPath.getPath(),singleton.sourcePath.getPath());
		}
		return singleton;
	}
	private  void checkFiles() throws MalformedURLException, UnsupportedEncodingException { 
		//Let's first look for the files in data
		String contextPath = URLDecoder.decode(CSVManager.contextPath,"UTF-8");
		File f = new File(contextPath);
	 FileStructure structure = new FileStructure(f,true);
	 List<String> dateNames = new ArrayList<String>();
	 
	 if(!structure.getFiles().isEmpty()){
		 String source ="";
		 String incremental ="";
		//Then we only get those with Date format. And get the newest as the incremental
		 for(String tem:structure.getFiles()){
			
			 DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			 Date newest = null;
			 try {
				 
				Date date = format.parse(tem.replace(".csv",""));
				dateNames.add(tem);
				
				if(newest == null||date.after(newest)){
					newest = date;
					incremental = tem;
				}
			} catch (ParseException e) {
				continue;
			}
		 }
		//Now we'll check how many of them there are, and get some metadata to identify them
		if(!dateNames.isEmpty()){
			if(dateNames.size()==1){
				 source = incremental ;
			}else if(dateNames.size()>=2){
				 long maxSize = 0;
				 //The heaviest file will be considered as the sourceFile;
				 for(String tem:dateNames){
					 File file = new File(contextPath+tem);
					 BasicFileAttributes attr;
					try {
						attr = Files.readAttributes(file.toPath(), BasicFileAttributes.class);
						 Long size =attr.size();
						 if(size>maxSize){
							 maxSize = size;
							 source = tem;
						 }
					} catch (IOException e) {
						continue;
					}
					
				 }
				 
			 }
		} 
		 sourcePath = preparePath(source);
		 incrementalPath = preparePath(incremental);
		 
	 }
		if(sourcePath == null){
			try {
				setSourcePath(CSVManager.outBasePath+"2017-06-08.csv");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(incrementalPath == null){
			try {
				Calendar today = Calendar.getInstance();
				String year = today.get(Calendar.YEAR)+"";
				String month = ""+(today.get(Calendar.MONTH)+1);
				month = today.get(Calendar.MONTH)+1>9?month:"0"+month;
				String day = ""+today.get(Calendar.DAY_OF_MONTH);
				day = today.get(Calendar.DAY_OF_MONTH)>9?day:"0"+day;
				
				String fileNameToday =""+year+"-"+month+"-"+day+".csv";
				setIncrementalPath(CSVManager.outBasePath+fileNameToday);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	private CSVManager(){
		super();
	}
	
	public URL getDefaultIncrementalPath() {
		return this.incrementalPath;
	}

	public void setIncrementalPath(String path) throws IOException  {
		String contextPath = URLDecoder.decode(CSVManager.contextPath,"UTF-8");
		if(path.contains("http")){
			String[] fileNames = path.split("[/]");
			String fileName = fileNames[fileNames.length-1];
			connectAndSave(path, contextPath+fileName);
			this.incrementalPath = preparePath(fileName);
		}else{
			URL fullPath;
			try {
				fullPath = preparePath(path);
			} catch (MalformedURLException e) {
				connectAndSave(path, contextPath+path);
				fullPath = preparePath(path);
			}
			this.incrementalPath = fullPath;
		}
		this.currentStatus.setLastUpdate(path);
	}

	public URL getDefaultSourcePath() {
		
		return sourcePath;
	}
	
	public void setSourcePath(String path) throws IOException {
		String contextPath = URLDecoder.decode(CSVManager.contextPath,"UTF-8");
		 if(path.contains("http")){
			
			String[] fileNames = path.split("[/]");
			String fileName = fileNames[fileNames.length-1];
			connectAndSave(path, contextPath+fileName);
			this.sourcePath = preparePath(fileName);
		}else{
			URL fullPath;
			try {
				fullPath = preparePath(path);
			} catch (MalformedURLException e) {
				connectAndSave(path, contextPath+path);
				fullPath = preparePath(path);
			}
			this.sourcePath = fullPath;
		}
		
		this.currentStatus.setLastSource(path);
	}
	
	
	public BDStatus getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(BDStatus currentStatus) {
		this.currentStatus = currentStatus;
	}
	
	public String getOutBasePath() {
		return outBasePath;
	}
	public void setOutBasePath(String outBasePath) {
		CSVManager.outBasePath = outBasePath;
	}
	private URL preparePath(String path) throws MalformedURLException, UnsupportedEncodingException{
		URL res;
		if(path.contains(PATHDETECTOR)){
			 res = new URL("file:/"+URLDecoder.decode(path, "UTF-8"));
		}else{
			String prePath = URLDecoder.decode(this.getClass().getClassLoader().getResource("data/"+path).getPath().replaceFirst("/",""), "UTF-8");
			res = new URL("file:/"+prePath);
				
		}
		return res;
	}

	public String incrementalMerge() {
		String result;
		result = null;
				
		Assert.notNull(this.incrementalPath);
		Assert.notNull(this.sourcePath);
		CSVUtils utility = new CSVUtils(sourcePath,incrementalPath);
		try {
			utility.mergeFiles();
		} catch (IOException e) {
			result = IOERROR;
			e.printStackTrace();
		}
		return result;
	}
	
	public static void connectToCRON(String fileName) throws IOException{
		
		connectAndSave(CSVManager.outBasePath+fileName, contextPath+fileName);
	}
	public static void connectAndSave(String webPath,String savePath) throws IOException{
		URL website = new URL(webPath);
		System.out.println( "retrieving from: "+webPath);
		ReadableByteChannel rbc = Channels.newChannel(website.openStream());
		FileOutputStream fos = new FileOutputStream(savePath);
		fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		
		fos.close();
		System.out.println("file saved in : "+savePath);
	}


}
