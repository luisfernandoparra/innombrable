/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.us.isa.markexplorer;

import javax.servlet.Filter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import io.swagger.api.ApiOriginFilter;

@Configuration
public class FiltersConfiguration {
    @Bean
    public FilterRegistrationBean someFilterRegistration() {

    FilterRegistrationBean registration = new FilterRegistrationBean();
    registration.setFilter(someFilter());
    registration.addUrlPatterns("/*");
    //registration.addInitParameter("paramName", "paramValue");
    registration.setName("ApiOriginFilter");
    registration.setOrder(1);
    return registration;
} 


public Filter someFilter() {
    return new ApiOriginFilter();
}
}
