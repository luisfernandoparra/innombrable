package utils;

import java.util.ArrayList;
import java.util.Collection;

public class MEStringUtils {

	
	public static Collection<String> clearEmpties(Collection<String> source){
		Collection<String> result = new ArrayList<String>();
		for(String te:source){
			if(!te.trim().equals("")){
				result.add(te);
			}
		}
		return result;
	}
	public static Collection<String> clearEmpties(String[] source){
		Collection<String> result = new ArrayList<String>();
		for(String te:source){
			if(!te.isEmpty()&&!te.trim().equals("")){
				result.add(te);
			}
		}
		return result;
	}
	
}
